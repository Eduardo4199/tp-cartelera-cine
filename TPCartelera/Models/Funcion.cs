﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.ComponentModel.DataAnnotations.Schema;

namespace TPCartelera.Models
{
    public class Funcion
    {
        public int ID { get; set; }
        public Pelicula Pelicula { get; set; }
        public int PeliculaID { get; set; }
        public Sala Sala { get; set; }
        public int SalaID { get; set; }
        [Column(TypeName = "DateTime2")]
        public DateTime HoraInicio { get; set; }
    }
}