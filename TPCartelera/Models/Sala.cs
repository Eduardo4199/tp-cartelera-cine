﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TPCartelera.Models
{

    public abstract class Sala
    {
        public int ID { get; set; }
        public int CineID { get; set; }
        public Cine Cine { get; set; }
        public string Descripcion { get; set; }
    }
}