﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace TPCartelera.Models
{
    public abstract class Pelicula 
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Sinopsis { get; set; }
        public int Duracion { get; set; }
        public string Imagen { get; set; }
    }
}