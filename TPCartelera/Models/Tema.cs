﻿namespace TPCartelera.Models
{
    public class Tema
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
    }
}