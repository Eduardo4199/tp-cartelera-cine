﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace TPCartelera.Models
{

    public class Cine
    {

        public int ID { get; set; }
        [Required]
        public string Nombre { get; set; }
        public ICollection<Sala> Sala { get; set; }
    }
}