﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TPCartelera.Models
{
    public class Documental : Pelicula
    {
        public Tema Tema { get; set; }
        [ForeignKey("Tema")]
        public int TemaID { get; set; }

    }
}