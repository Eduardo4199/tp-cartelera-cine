﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPCartelera.Models
{
    public class User
    {
        public int ID { get; set; }
        public string Usuario { get; set; }
        public string Contraseña { get; set; }
    }
}