﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TPCartelera.Models;

namespace TPCartelera.DAL
{
    public class CineContext : DbContext
    {
        public CineContext() : base("TP2-Corzo")
        {
            Database.SetInitializer(new CineInitializer());
        }
        public DbSet<Cine> Cine { get; set; }
        public DbSet<Sala> Sala { get; set; }
        public DbSet<Funcion> Funcion { get; set; }
        public DbSet<Pelicula> Pelicula { get; set; }
        public DbSet<Tema> Tema { get; set; }
        public DbSet<User> Users { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public System.Data.Entity.DbSet<TPCartelera.Models.Comercial> Comercials { get; set; }

        public System.Data.Entity.DbSet<TPCartelera.Models.Documental> Documentals { get; set; }
    }
}