﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TPCartelera.DAL;
using TPCartelera.Models;
using TPCartelera.ViewModels;

namespace TPCartelera.Controllers
{
    public class HomeController : Controller
    {
        private CineContext db = new CineContext();
        public ActionResult Index()
        {
            IndexView index = new IndexView();
            index.Pelicula = db.Pelicula.ToList();
            index.Cine = db.Cine.ToList();
            return View(index);
        }

        public ActionResult BuscarFuncion(FormCollection formCollection)
        {
            var IdCine = Int32.Parse(formCollection["idcine"]);
            var IdPel = Int32.Parse(formCollection["idpel"]);
            BuscarView funcion = new BuscarView();
            if (IdCine == 0)
            {
                funcion.Salas = db.Sala.Include("Cine").ToList();
                funcion.Funciones = db.Funcion.Include("Sala").Where(i => i.PeliculaID == IdPel).ToList();
            }
            else
            {
                funcion.Salas = db.Sala.Include("Cine").Where(i => i.CineID == IdCine).ToList();
                funcion.Funciones = db.Funcion.Include("Sala").Where(i => i.PeliculaID == IdPel && i.Sala.CineID == IdCine).ToList();
            }
            funcion.Cines = db.Cine.Include("Sala");
            funcion.Pelicula = db.Pelicula.Single(i => i.ID == IdPel);
            funcion.Temas = db.Tema.ToList();
            return View(funcion);
        }
        public ActionResult BusquedaPersonalizada(FormCollection formCollection)
        {
            var texto = formCollection["texto"].ToLower();
            var cine = Int32.Parse(formCollection["cine"]);
            BusquedaPersonalizada busq = new BusquedaPersonalizada();
            busq.Cine = cine;
            if (cine == 0)
            {
                var aux = db.Pelicula.ToList();
                IList<Pelicula> Busqueda = new List<Pelicula>();
                foreach (var item in aux)
                {
                    if (BuscadorGeneral(item, texto) > 0)
                    {
                        Busqueda.Add(item);
                    }
                }
                busq.Peliculas = Busqueda.ToList();
            }
            else
            {
                var aux = db.Funcion.Include("Sala").Include("Pelicula").Where(i => i.Sala.CineID == cine).ToList();
                IList<Pelicula> Funcion = new List<Pelicula>();
                foreach(var item in aux)
                {
                    if(BuscadorGeneral(item.Pelicula, texto)> 0)
                    {
                        if (!Funcion.Contains(item.Pelicula))
                        {
                            Funcion.Add(item.Pelicula);
                        }
                    }
                }
                busq.Peliculas = Funcion.ToList();
            }

            return View(busq);
        }
        private int BuscadorGeneral(Pelicula pelicula, string texto)
        {
            int contAux = 0;
            string titulo = pelicula.Nombre.ToLower();
            if (titulo.Contains(texto))
            {
                contAux++;
            }
            string sinopsis = pelicula.Sinopsis.ToLower();
            if (sinopsis.Contains(texto))
            {
                contAux++;
            }
            if (BusquedaEspecifica(pelicula, texto))
            {
                contAux++;
            }
            return contAux;
        }
        private bool BusquedaEspecifica(Pelicula item, string texto)
        {
            if (item is Comercial)
            {
                Comercial comercial = item as Comercial;
                string actores = comercial.Actores.ToLower();
                if (actores.Contains(texto))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                Documental documental = item as Documental;
                var tema = db.Tema.Single(i => i.ID == documental.TemaID);
                string temaBusq = tema.Nombre.ToLower();
                if (temaBusq.Contains(texto))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}   