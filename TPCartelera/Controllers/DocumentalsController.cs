﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TPCartelera.DAL;
using TPCartelera.Filters;
using TPCartelera.Models;

namespace TPCartelera.Controllers
{
    [AuthFilter]
    public class DocumentalsController : Controller
    {
        private CineContext db = new CineContext();

        // GET: Documentals/Create
        public ActionResult Create()
        {
            return View(db.Tema.ToList());
        }

        // POST: Documentals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection formCollection, HttpPostedFileBase img)
        {
            Documental documental = new Documental();
            documental.Nombre = formCollection["nombre"];
            documental.Sinopsis = formCollection["sinopsis"];
            documental.Duracion = Int32.Parse(formCollection["Duracion"]);
            documental.Imagen = ConvertirABase64(img);
            documental.Tema = GetTemaById(Int32.Parse(formCollection["temaid"]));
            if (documental.Duracion > 360)
            {
                ViewBag.Error = "La duracion es mas de 360 minutos!";
                return View("Error");
            }
            db.Pelicula.Add(documental);
            db.SaveChanges();
            return RedirectToAction("Index", "Peliculas");
        }
        private string ConvertirABase64(HttpPostedFileBase img)
        {
            var fileStream = img.InputStream;
            var binaryReader = new BinaryReader(fileStream);
            var bytes = binaryReader.ReadBytes((Int32)fileStream.Length);
            var base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
            return "data:image/png;base64," + base64String;
        }

        private Tema GetTemaById(int id)
        {
            var aux = db.Tema.ToList().Single(i => i.ID == id);
            return aux;
        }
       
    }
}
