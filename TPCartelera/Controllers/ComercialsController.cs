﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TPCartelera.DAL;
using TPCartelera.Filters;
using TPCartelera.Models;

namespace TPCartelera.Controllers
{
    [AuthFilter]
    public class ComercialsController : Controller
    {
        private CineContext db = new CineContext();

        public ActionResult Create()
        {
            return View();
        }

        // PO                                                                                               ST: Comercials/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection formCollection, HttpPostedFileBase img)
        {
            Comercial comercial = new Comercial();
            comercial.Nombre = formCollection["nombre"];
            comercial.Sinopsis = formCollection["sinopsis"];
            comercial.Duracion = Int32.Parse(formCollection["duracion"]);
            comercial.Actores = formCollection["actores"];
            comercial.Imagen = ConvertirABase64(img);
            if(comercial.Duracion > 360)
            {
                ViewBag.Error = "La duracion es mas de 360 minutos!";
                return View("Error");
            }
            db.Pelicula.Add(comercial);
            db.SaveChanges();
            return RedirectToAction("Index","Peliculas");
        }

        // GET: Comercials/Edit/5
        private string ConvertirABase64(HttpPostedFileBase img)
        {
            var fileStream = img.InputStream;
            var binaryReader = new BinaryReader(fileStream);
            var bytes = binaryReader.ReadBytes((Int32)fileStream.Length);
            var base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
            return "data:image/png;base64," + base64String;
        }

    }
}
