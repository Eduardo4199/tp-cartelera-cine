﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TPCartelera.DAL;
using TPCartelera.Models;

namespace TPCartelera.Controllers
{
    public class UsersController : Controller
    {
        private CineContext db = new CineContext();

        public ActionResult Login()
        {
            return View("Login");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string usuario, string contraseña)
        {
            try
            {
                var user = db.Users.Single(u => u.Usuario == usuario && u.Contraseña == contraseña);
                Session["Usuario"] = user;
                return RedirectToAction("Index", "Home");
            }
            catch (Exception e)
            {
                ViewBag.Error = "Usuario o contraseña incorrectos";
                return View("Error");
            }
            /*var user = db.Users.Single(u => u.Usuario == usuario);
            var pass = db.Users.Single(u => u.Contraseña == contraseña);
            if(user != null && pass != null)
            {
                Session["Usuario"] = usuario;
                return RedirectToAction("Index","Home");
            }
            else
            {
                return View("Error");
            }*/

        }
        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index","Home");
        }
    }
}
