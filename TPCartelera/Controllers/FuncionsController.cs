﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TPCartelera.DAL;
using TPCartelera.Filters;
using TPCartelera.Models;
using TPCartelera.ViewModels;

namespace TPCartelera.Controllers
{
    [AuthFilter]
    public class FuncionsController : Controller
    {
        private CineContext db = new CineContext();

        // GET: Funcions
        public ActionResult Index()
        {
            FuncionsIndexView funcions = new FuncionsIndexView();
            funcions.Funcions = db.Funcion.Include("Pelicula").Include("Sala").ToList();
            funcions.Cines = db.Cine.ToList();
            funcions.Salas = db.Sala.ToList();
            return View(funcions);
        }
        public ActionResult Error()
        {
            return View();
        }
        // GET: Funcions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funcion funcion = db.Funcion.Find(id);
            if (funcion == null)
            {
                return HttpNotFound();
            }
            return View(funcion);
        }

        // GET: Funcions/Create
        public ActionResult Create()
        {
            FuncionView Funcion = new FuncionView();
            Funcion.Pelicula = db.Pelicula.ToList();
            Funcion.Sala = db.Sala.Include("Cine");
            return View(Funcion);
        }

        // POST: Funcions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection formCollection)
        {
            Funcion funcion = new Funcion();
            var horaFun = DateTime.Now.Date;
            funcion.HoraInicio = horaFun;
            DateTime aux = horaFun.Date;
            var idSala = Int32.Parse(formCollection["sala"]);
            DateTime fechaConv = ComprobarHorario(horaFun,idSala);
           if (DateTime.Compare(fechaConv,aux) > 0)
            {
                funcion.HoraInicio = fechaConv;
            }
            else
            {
                ViewBag.Error = "Ocurrio un problema guardando la funcion";
                return View("Error");
            }

            funcion.Sala = db.Sala.Single(s => s.ID == idSala);
            var idPel = Int32.Parse(formCollection["pelicula"]);
            funcion.Pelicula = db.Pelicula.Single(p => p.ID == idPel);
            if(funcion.Sala is Models.HD && funcion.Pelicula is Models.Documental)
            {
                ViewBag.Error = "Un documental no puede estar en una sala de Alta Definición";
                return View("Error");
            }
            db.Funcion.Add(funcion);
            db.SaveChanges();
            return RedirectToAction("Index", "Funcions");
        }

        private DateTime ComprobarHorario(DateTime horaFun, int SalaID)
        {

            var funciones = db.Funcion.ToList()
                .Where(i => i.HoraInicio.Date >= horaFun.Date && i.HoraInicio <= horaFun.Date.AddDays(1).AddHours(2))
                .Where(i => i.SalaID == SalaID);
            var hora = horaFun.Date.AddHours(10);
            var horaMax = hora.Date.AddDays(1).AddHours(2);
            if (funciones != null)
            {
                foreach (var item in funciones)
                {
                   
                    Pelicula pelicula = db.Pelicula.Single(i => i.ID == item.PeliculaID);
                    double minutosAdd = 15.0;
                    hora = hora.AddMinutes(pelicula.Duracion);
                    hora = hora.AddMinutes(minutosAdd);
                    
                }
                /*
                Pelicula pelicula = funciones.FirstOrDefault().Pelicula;
                hora = hora.AddMinutes(pelicula.Duracion);*/
                if(DateTime.Compare(hora,horaMax) == -1)
                {
                    return hora;
                }
                else
                {
                    DateTime aux = new DateTime();
                    return aux;
                }
            }
            else
            {
                return hora;
            }
        }

        // GET: Funcions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funcion funcion = db.Funcion.Find(id);
            if (funcion == null)
            {
                return HttpNotFound();
            }
            return View(funcion);
        }

        // POST: Funcions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection formCollection)
        {
            Funcion funcion = new Funcion();
            var horaFun = Convert.ToDateTime(formCollection["hora"]);
            if (DateTime.Compare(horaFun,Convert.ToDateTime("10:00:00")) > 0 && DateTime.Compare(horaFun,Convert.ToDateTime("2:00:00")) < 0)
            {
                funcion.HoraInicio = horaFun;
            }
            else
            {
                ViewBag.Error = "Ocurrio un problema guardando la funcion";
                return View("Error");
            }
            var idPel = Int32.Parse(formCollection["pelicula"]);
            funcion.Pelicula = db.Pelicula.Single(p => p.ID == idPel);
            var idSala = Int32.Parse(formCollection["sala"]);
            funcion.Sala = db.Sala.Single(s => s.ID == idSala);
            return View();
        }

        // GET: Funcions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funcion funcion = db.Funcion.Find(id);
            if (funcion == null)
            {
                return HttpNotFound();
            }
            return View(funcion);
        }

        // POST: Funcions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Funcion funcion = db.Funcion.Find(id);
            db.Funcion.Remove(funcion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult BorrarFunciones(FormCollection formCollection)
        {
            var id = Int32.Parse(formCollection["id"]);
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var funciones = db.Funcion.Where(i => i.SalaID == id).ToList();
            if( funciones.Count() == 0)
            {
                ViewBag.Error = "No existe ninguna funcion en esta sala";
                return View("Error");
            }
            else
            {
                db.Funcion.RemoveRange(funciones);
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }
        public ActionResult ElegirCine()
        {
            return View(db.Cine.ToList());
        }
    }
}
