﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TPCartelera.DAL;
using TPCartelera.Filters;
using TPCartelera.Models;

namespace TPCartelera.Controllers
{
    [AuthFilter]
    public class PeliculasController : Controller
    {
        private CineContext db = new CineContext();

        // GET: Peliculas
        public ActionResult Index()
        {
            return View(db.Pelicula.ToList());
        }

        // GET: Peliculas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Peliculas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Nombre,Sinopsis,Duracion,Imagen")] Pelicula pelicula)
        {
            if (ModelState.IsValid)
            {
                db.Pelicula.Add(pelicula);
                db.SaveChanges();
                return RedirectToAction("Peliculas/Index");
            }

            return View(pelicula);
        }

        // GET: Peliculas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pelicula pelicula = db.Pelicula.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        // POST: Peliculas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pelicula pelicula = db.Pelicula.Find(id);
            IEnumerable<Funcion> funciones = db.Funcion.Where(i => i.PeliculaID == pelicula.ID).ToList();
            if(funciones == null || funciones.Count() == 0)
            {
                db.Pelicula.Remove(pelicula);
                db.SaveChanges();
            }
            else
            {
                ViewBag.Error = "La pelicula esta programada / o siendo exhibida en una funcion";
                return View("Error");
            }
            return RedirectToAction("Index");
        }
        public ActionResult ElegirPelicula()
        {
            return View();
        }
    }
}
