﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TPCartelera.Models;

namespace TPCartelera.ViewModels
{
    public class BuscarView
    {
        public IEnumerable<Cine> Cines{ get; set; }
        public IEnumerable<Funcion> Funciones  { get; set; }
        public IEnumerable<Sala> Salas { get; set; }
        public IEnumerable<Tema> Temas { get; set; }
        public Pelicula Pelicula { get; set; }
    }
}