﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TPCartelera.Models;

namespace TPCartelera.ViewModels
{
    public class IndexView
    {
        public IEnumerable<Pelicula> Pelicula { get; set; }
        public IEnumerable <Cine> Cine{ get; set; }
    }
}