﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TPCartelera.Models;

namespace TPCartelera.ViewModels
{
    public class BusquedaPersonalizada
    {
        public IEnumerable<Pelicula> Peliculas { get; set; }

        public int Cine { get; set; }
    }
}