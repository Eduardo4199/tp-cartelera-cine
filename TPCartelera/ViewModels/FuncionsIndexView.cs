﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TPCartelera.Models;

namespace TPCartelera.ViewModels
{
    public class FuncionsIndexView
    {
        public IEnumerable<Funcion> Funcions { get; set; }

        public IEnumerable <Cine> Cines { get; set; }

        public IEnumerable<Sala> Salas { get; set; }
    }
}