﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TPCartelera.Models;

namespace TPCartelera.ViewModels
{
    public class FuncionView
    {
        public IEnumerable<Pelicula> Pelicula { get; set; }

        public IEnumerable<Sala> Sala { get; set; }

    }
}